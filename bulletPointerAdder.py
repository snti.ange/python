# PASSWORDS = {
#     'email':'alksjdflkajsdf',
#     'blog' : 'alkjsdflkjasdf',
#     'luggage': '12345'
# }

# import sys, pyperclip

# if len(sys.argv) < 2:
#     print('nothing passed')
#     sys.exit()

# account = sys.argv[1]

# if account in PASSWORDS:
#     pyperclip.copy(PASSWORDS[account])
#     print('Password of ', account, ' added to buffer')
#     print(pyperclip.paste())
# else :
#     print('Account not found')

import pyperclip

text = pyperclip.paste()

lines = text.split('\n')

for i in range(len(lines)):
    lines[i] = '* ' + lines[i]

text = '\n'.join(lines)

print(text)
pyperclip.copy(text)
