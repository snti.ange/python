
def addToInventory(inv, addedItems):
    for item in addedItems:
        inv.setdefault(item, 0)
        inv[item] += 1
    return inv

inventory = {'rope': 11, 'dagger': 10}
dragonLoot = ['gold coin', 'dagger', 'gold coin', 'gold coin']

print(inventory)
inventory = addToInventory(inventory, dragonLoot)
print(inventory)