def printTable(list):
    colWidth = len(list[0])
    for i in range(colWidth):
        print(list[colWidth][i] + '\t\t' + list[1][i] + '\t\t' + list[2][i])


tableData = [['apples', 'oranges', 'cherries', 'banana'],
             ['Alice', 'Bob', 'Carol', 'David'],
             ['dogs', 'cats', 'moose', 'goose']]

v = [0] * len(tableData)
print(v)
printTable(tableData)
