inventory = {'rope': 1, 'torch': 6, 'gold coin': 42}


def displayInventory(inv):
    print ('Inventory:')
    total = 0
    for k, v in inv.items():
        print(str(v) + ' => ' + k)
        total = total + v
    print('Total number of items: ' + str(total))

displayInventory(inventory)